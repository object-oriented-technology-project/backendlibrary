package too.unam.ToDoApp.Interface;

import too.unam.ToDoApp.Model.Libro;

import java.util.List;

public interface InterfaceServiceLibro extends InterfaceGenericService<Libro>{

    /**
     * PARA USO DE LOS USUARIOS CON ROL PRESTADOR.
     * Regresa una lista de libros paginados.
     * Filtra por nombre de libro y de autor.
     * Trae solo los libros que pertenecen al username
     * @param username
     * @param page
     * @param book
     * @param author
     * @return Lista de libros paginados pertenecientes al prestador.
     */
    List<Libro> readCataloguePaginatedLender(String username, int page, String book, String author);

    /**
     * PARA USO DE LOS USUARIOS CON ROL CONSUMIDOR.
     * Regresa una lista de libros paginados.
     * Filtra por nombre de libro y de autor.
     * @param page
     * @param book
     * @param author
     * @return Lista de libros paginados
     */
    List<Libro> readCataloguePaginatedConsumer(int page, String book, String author);
}
