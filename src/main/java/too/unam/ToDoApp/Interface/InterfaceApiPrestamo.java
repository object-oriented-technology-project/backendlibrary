package too.unam.ToDoApp.Interface;

import org.springframework.http.ResponseEntity;
import too.unam.ToDoApp.Model.DTO.Request.RequestPrestamo;

public interface InterfaceApiPrestamo {

    ResponseEntity requestLend(RequestPrestamo requestPrestamo);
    ResponseEntity acceptLend(RequestPrestamo requestPrestamo);
    ResponseEntity rejectLend(RequestPrestamo requestPrestamo);
    ResponseEntity returnBook(RequestPrestamo requestPrestamo);
    ResponseEntity rateConsumer(RequestPrestamo requestPrestamo);
    ResponseEntity rateBook(RequestPrestamo requestPrestamo);
    ResponseEntity getAll();
}
