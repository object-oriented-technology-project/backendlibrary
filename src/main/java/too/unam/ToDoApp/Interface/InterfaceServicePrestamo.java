package too.unam.ToDoApp.Interface;

import too.unam.ToDoApp.Model.DTO.Util.User;
import too.unam.ToDoApp.Model.Prestamo;

import java.util.List;

public interface InterfaceServicePrestamo extends InterfaceGenericService<Prestamo> {

    /**
     * SOLO PARA USO DE USUARIOS CON ROL CONSUMIDOR
     * Metodo que regresa todos los Prestamos que un consumidor ha pedido.
     * @param username
     * @return Lista de Prestamos.
     */
    List<Prestamo> getAllPrestamosConsumer(String username);

    /**
     * SOLO PARA USO DE USUARIOS CON ROL PRESTADOR
     * Metodo que regresa todos los Prestamos que se han realizado a libros de un Prestador.
     * @param username
     * @return Lista de Prestamos
     */
    List<Prestamo> getAllPrestamosLender(String username);
}
