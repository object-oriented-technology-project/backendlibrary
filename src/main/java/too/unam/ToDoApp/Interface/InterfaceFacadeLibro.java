package too.unam.ToDoApp.Interface;

import too.unam.ToDoApp.Model.Libro;

public interface InterfaceFacadeLibro {

    /**
     * Este metodo no tiene ni una validacion para leer un libro.
     * Por lo que su funcionamiento debe ser interno a la aplicacion.
     * Por ni una razon el cliente debe ser capaz de llamar a este metodo.
     * @param id
     * @return
     */
    public Libro unsecureRead(int id);

    /**
     * No se realiza ni una validacion para calificar el libro.
     * Por lo que solo debe usarse este metodo cuando se sabe lo pasa.
     * Por ni una razon el cliente debe ser capaz de llamar a este metodo.
     * @param libro
     * @param calificacion
     */
    public void unsecureCalificarLibro(Libro libro, int calificacion);
}
