package too.unam.ToDoApp.Interface;

import java.util.List;

public interface InterfaceGenericService<T> {

    /**
     * Metodo generico.
     * Actualmente sirve para las clases: Calificacion, Libro y Prestamo.
     * Realiza la lectura de estas clases.
     * @param id
     * @return Una entidad.
     */
    public T read(int id);

    /**
     * Metodo generico.
     * Actualmente sirve para las clases: Calificacion, Libro y Prestamo.
     * Lee todas las entidades de estas clases.
     * @return Lista completa de las entidades.
     */
    public List<T> readAll();

    /**
     * Metodo generico.
     * Actualmente sirve para las clases: Calificacion, Libro y Prestamo.
     * Guarda las entidades de estas clases.
     * @param entity
     */
    public void save(T entity);

    /**
     * Metodo generico.
     * Actualmente sirve para las clases: Calificacion, Libro y Prestamo.
     * Actualiza las entidades de estas clases.
     * @param entity
     */
    public void update(T entity);

    /**
     * Metodo generico.
     * Actualmente sirve para las clases: Calificacion, Libro y Prestamo.
     * Elimina las entidades de estas clases.
     * @param entity
     */
    public void delete (T entity);
}
