package too.unam.ToDoApp.Interface;

import too.unam.ToDoApp.Model.Calificacion;

import java.util.List;

public interface InterfaceFacadeCalificacion {

    void rate(String consumidorUsername, int calificacionConsumidor);
    Calificacion get(String consumidorUsername);
    void create(String consumidorUsername);
    List<Integer> getListaCalificaciones(List<String> consumers);
}
