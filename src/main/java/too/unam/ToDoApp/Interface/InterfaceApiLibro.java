package too.unam.ToDoApp.Interface;

import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;
import too.unam.ToDoApp.Model.DTO.Request.RequestLibro;

public interface InterfaceApiLibro {

    ResponseEntity create(RequestLibro requestLibro, MultipartFile file);
    ResponseEntity update (RequestLibro requestLibro, MultipartFile file);
    ResponseEntity delete (RequestLibro requestLibro);
    ResponseEntity read (int id);
    ResponseEntity readCataloguePaginated(RequestLibro requestLibro);
}
