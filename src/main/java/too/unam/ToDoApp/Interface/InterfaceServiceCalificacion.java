package too.unam.ToDoApp.Interface;

import too.unam.ToDoApp.Model.Calificacion;

public interface InterfaceServiceCalificacion extends InterfaceGenericService<Calificacion> {

    /**
     * La llave primaria de Calificacion es un String.
     * Este metodo regresa la Calificacion cuya llave primaria es username.
     * @param username
     * @return
     */
    Calificacion read (String username);
}
