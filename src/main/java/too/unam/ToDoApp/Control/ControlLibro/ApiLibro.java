package too.unam.ToDoApp.Control.ControlLibro;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.server.ResponseStatusException;
import too.unam.ToDoApp.Interface.InterfaceApiLibro;
import too.unam.ToDoApp.Model.DTO.Request.RequestLibro;
import too.unam.ToDoApp.Model.DTO.Response.ResponseLibroCatalogue;
import too.unam.ToDoApp.Model.DTO.Util.User;
import too.unam.ToDoApp.Model.Libro;


import javax.servlet.MultipartConfigElement;
import javax.validation.Valid;
import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/libros")
public class ApiLibro implements InterfaceApiLibro {

    @Autowired
    private ControllerLibro controllerLibro;

    @Bean
    public MultipartConfigElement multipartConfigElement() {
        return new MultipartConfigElement("");
    }

    @Bean
    public MultipartResolver multipartResolver() {
        org.springframework.web.multipart.commons.CommonsMultipartResolver multipartResolver = new org.springframework.web.multipart.commons.CommonsMultipartResolver();
        multipartResolver.setMaxUploadSize(1000000);
        return multipartResolver;
    }

    @RequestMapping(value = {"/hello"}, method = RequestMethod.GET, consumes = {"multipart/form-data"})
    public ResponseEntity hello() {
        return ResponseEntity.ok("Hello World");
    }

    /**
     * Precondiciones: Rol Prestador.
     * Poscondiciones: Nuevo libro dado de alta.
     * Detalle: Se desea dar de alta un libro
     * @param requestLibro
     * @return Ok si la accion se realizo con exito
     */
    @Override
    @RequestMapping(value = {"/libro"}, method = RequestMethod.POST, consumes = "multipart/form-data")
    public ResponseEntity create(@RequestPart("properties") @Valid RequestLibro requestLibro, @RequestPart(name = "file", required = false) @Valid MultipartFile file) {
        try {
            final User user = getUser();
            controllerLibro.create(requestLibro, user, file);
        } catch (ResponseStatusException exc) {
            throw exc;
        } catch (Exception exc){
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "UNDEFINED ERROR " +exc.getMessage());
        }
        return ResponseEntity.ok("Book created");
    }

    /**
     * Detalle: Se desea actualizar un libro.
     * Precondiciones: Rol Prestado; Usuario dueño del libro.
     * Poscondiciones: Libro actualizado
     * @param requestLibro
     * @return Ok si la accion se realizo con exito
     */
    @Override
    @RequestMapping(value = {"/libro"}, method = RequestMethod.PUT, consumes = "multipart/form-data")
    public ResponseEntity update(@RequestPart("properties") @Valid RequestLibro requestLibro, @RequestPart(name = "file", required = false) MultipartFile file) {
        String urlImage="";
        try {
            final User user = getUser();
            urlImage = controllerLibro.update(requestLibro, user, file);
        } catch (ResponseStatusException exc) {
            throw exc;
        } catch (Exception exc){
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "UNDEFINED ERROR " +exc.getMessage());
        }
        return ResponseEntity.ok(urlImage);
    }


    /**
     * Detalle: Se desea eliminar un libro.
     * Precondiciones: Rol Prestado; Usuario dueño del libro.
     * Poscondiciones: Libro eliminado
     * @param requestLibro
     * @return Ok si la accion se realizo con exito
     */
    @Override
    @RequestMapping(value = {"/libro"}, method = RequestMethod.DELETE)
    public ResponseEntity delete(@RequestBody RequestLibro requestLibro) {
        try {
            final User user = getUser();
            controllerLibro.delete(requestLibro, user);
        } catch (ResponseStatusException exc) {
            throw exc;
        } catch (Exception exc){
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "UNDEFINED ERROR " +exc.getMessage());
        }
        return ResponseEntity.ok("Book deleted");
    }

    /**
     * Detalle: Se desea leer el detalle de un libro
     * Precondiciones: Rol Prestador - El libro debe ser de su propiedad; Cualquier otro caso no hay condiciones.
     * Poscondiciones: El usuario recibe el libro
     * @param id
     * @return Libro
     */
    @Override
    @RequestMapping(value = {"/libro/{id}"}, method = RequestMethod.GET)
    public ResponseEntity read(@PathVariable(value = "id") int id) {
        try {
            RequestLibro requestLibro = new RequestLibro();
            requestLibro.setId(id);
            final User user = getUser();
            Libro libro = controllerLibro.read(requestLibro, user);
            return ResponseEntity.ok(libro);
        } catch (ResponseStatusException exc) {
            throw exc;
        } catch (Exception exc){
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "UNDEFINED ERROR " +exc.getMessage());
        }
    }

    /**
     * Detalle: Se desea obtener el catalogo de libros
     * Precondiciones: NA
     * Poscondiciones: El usuario recibe el catalogo de libros. El prestador solo recibe los libros
     * que son de su propiedad. En cualquier otro caso pueden recibir cualquier libro.
     * @param requestLibro
     * @return Lista de libros
     */
    @Override
    @RequestMapping(value={"/catalogo"}, method = RequestMethod.POST)
    public ResponseEntity readCataloguePaginated(@RequestBody RequestLibro requestLibro) {
        try {
            final User user = getUser();
            List<ResponseLibroCatalogue> libros = controllerLibro.readCataloguePaginated(requestLibro, user);
            return ResponseEntity.ok(libros);
        } catch (ResponseStatusException exc) {
            throw exc;
        } catch (Exception exc){
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "UNDEFINED ERROR " +exc.getMessage());
        }
    }

    private User getUser(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        final String username = (String) authentication.getPrincipal();
        final Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
        GrantedAuthority authority = (GrantedAuthority)authorities.toArray()[0];
        return new User(username, authority.getAuthority());
    }
}
