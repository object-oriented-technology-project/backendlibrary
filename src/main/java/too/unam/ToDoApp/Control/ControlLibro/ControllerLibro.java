package too.unam.ToDoApp.Control.ControlLibro;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import too.unam.ToDoApp.Interface.InterfaceServiceLibro;
import too.unam.ToDoApp.Model.DTO.Request.RequestLibro;
import too.unam.ToDoApp.Model.DTO.Response.ResponseLibroCatalogue;
import too.unam.ToDoApp.Model.DTO.Util.User;
import too.unam.ToDoApp.Model.Libro;
import too.unam.ToDoApp.Util.JwtUtil;
import too.unam.ToDoApp.Util.Util;

import java.io.File;
import java.util.List;
import java.util.Map;

@Service
public class ControllerLibro {

    @Autowired
    private InterfaceServiceLibro serviceLibro;

    /**
     * Metodo que se encarga de crear un nuevo libro
     * @param requestLibro
     * @param user
     */
    public void create(RequestLibro requestLibro, User user, MultipartFile multipartFile) {
        if(create_Validation(requestLibro, user)){

            String portadaImagenUrl = addFile(multipartFile);

            Libro libro = new Libro(
                    requestLibro.getNombre(),
                    requestLibro.getAutor(),
                    requestLibro.getContraportada(),
                    portadaImagenUrl,
                    user.getUsername(),
                    requestLibro.getPrecioDia(),
                    0,
                    0
            );
            serviceLibro.save(libro);
        }
    }

    /**
     * Metodo que se encarga de actualizar un libro
     * @param requestLibro
     * @param user
     */
    public String update(RequestLibro requestLibro, User user, MultipartFile file) {
        Libro libro = serviceLibro.read(requestLibro.getId());

        String portadaImagenUrl = libro.getPortadaImagenUrl();
        if(file != null) {
            portadaImagenUrl = addFile(file);
        }

        if(update_Validation(requestLibro, user, libro)){
            libro.setNombre(requestLibro.getNombre());
            libro.setAutor(requestLibro.getAutor());
            libro.setContraportada(requestLibro.getContraportada());
            libro.setPortadaImagenUrl(portadaImagenUrl);
            libro.setPrecioDia(requestLibro.getPrecioDia());
            serviceLibro.update(libro);
        }

        return portadaImagenUrl;
    }

    private String addFile(MultipartFile file){
            Cloudinary cloudinary = new Cloudinary(ObjectUtils.asMap(
                    "cloud_name", "dssaljol0",
                    "api_key", "459936473351559",
                    "api_secret", "Fgv2VRH3oysV70zOkxW2Wi9TeL4"));
            Map uploadResult = null;
            try {
                if(file != null) {
                    uploadResult = cloudinary.uploader().upload(file.getBytes(), ObjectUtils.emptyMap());
                }
            } catch (Exception e) {

            }
        String portadaImagenUrl = "https://www.sanborns.com.mx/imagenes-sanborns-ii/1200/9786124076633.jpg";
        if (uploadResult != null){
            portadaImagenUrl = (String) uploadResult.get("url");
            if(portadaImagenUrl == null){
                portadaImagenUrl = "https://www.sanborns.com.mx/imagenes-sanborns-ii/1200/9786124076633.jpg";
            }
        }
        return portadaImagenUrl;
    }

    /**
     * Metodo que se encarga de eliminar un libro
     * @param requestLibro
     * @param user
     */
    public void delete(RequestLibro requestLibro, User user) {
        Libro libro = serviceLibro.read(requestLibro.getId());
        if(delete_Validation(user,libro)){
            serviceLibro.delete(libro);
        }
    }

    /**
     * Metodo que se encarga de leer un libro
     * @param requestLibro
     * @param user
     * @return
     */
    public Libro read(RequestLibro requestLibro, User user) {
        Libro libro = serviceLibro.read(requestLibro.getId());
        if(read_Validation(user, libro)){
            return libro;
        }
        return null;
    }

    /**
     * Metodo que se encarga de leer todos los libros
     * @param requestLibro
     * @param user
     * @return Lista de ResponseLibroCatalogue
     */
    public List<ResponseLibroCatalogue> readCataloguePaginated(RequestLibro requestLibro, User user) {
        if(readAll_Validation(requestLibro, user)){
            List<Libro> libros = null;
            if(!user.getRole().equals(JwtUtil.LENDER_ROLE)){
                libros = serviceLibro.readCataloguePaginatedConsumer(
                        requestLibro.getPagina(),
                        requestLibro.getNombre(),
                        requestLibro.getAutor());
            } else if (user.getRole().equals(JwtUtil.LENDER_ROLE)){
                libros = serviceLibro.readCataloguePaginatedLender(
                        user.getUsername(),
                        requestLibro.getPagina(),
                        requestLibro.getNombre(),
                        requestLibro.getAutor());
            } else {
                throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
            }
            if(libros!=null){
                List<ResponseLibroCatalogue> librosCatalogue = ResponseLibroCatalogue.LibrosToResponseLibroCatalogue(libros);
                return librosCatalogue;
            }
        }
        return null;
    }

    /**
     * Metodo que lee un libro (sin importar quien sea el usuario).
     * Solo debe ser utilizado por FacadeLibro
     * @param id
     * @return Libro.
     */
    public Libro unsecureRead(int id) {
        return serviceLibro.read(id);
    }

    /**
     * Metodo que califica un libro (sin importar quien sea el usuario)
     * Solo debe ser utilizado por FacadeLibro
     * @param libro
     * @param calificacion
     */
    public void unsecureCalificarLibro(Libro libro, int calificacion) {
        if(validateCalificacion(calificacion)){
            libro.setContadorCalificacion(libro.getContadorCalificacion()+1);
            libro.setSumatoriaCalificacion(libro.getSumatoriaCalificacion()+calificacion);
            serviceLibro.update(libro);
        }
    }

    //.................................. METODOS PRIVADOS .............................................................

    /**
     * Metodo que se encarga de validar la llamada al metodo create
     * @param requestLibro
     * @param user
     * @return true si es valido crear el libro
     */
    private boolean create_Validation(RequestLibro requestLibro, User user) throws ResponseStatusException{
        String errorMessage = "Invalid";
        if(!create_update_RequiredValues(requestLibro, errorMessage)){
            throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE);
        }
        if(!Util.isLender(user)){
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
        }
        return true;
    }

    /**
     * Metodo que se encarga de validar la llamada al metodo update
     * @param requestLibro
     * @param user
     * @param libro
     * @return true si es valido actualizar el libro
     */
    private boolean update_Validation(RequestLibro requestLibro, User user, Libro libro) throws ResponseStatusException{
        if(libro == null){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        if(!Util.isOwnerOfBook(user, libro)){
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
        }
        String errorMessage = "Invalid";
        if(!create_update_RequiredValues(requestLibro, errorMessage)){
            throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE);
        }

        return true;
    }

    /**
     * Metodo que se encarga de validar la llamada al metodo delete
     * @param user
     * @param libro
     * @return true si es valido eliminar el libro
     */
    private boolean delete_Validation(User user, Libro libro){
        if(libro == null){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        if(!Util.isOwnerOfBook(user, libro)){
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
        }
        return true;
    }

    /**
     * Metodo que se encarga de validar la llamada al metodo read
     * @param user
     * @param libro
     * @return true si es valido
     */
    private boolean read_Validation(User user, Libro libro){
        if(libro == null){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        if(user.getRole().equals(JwtUtil.LENDER_ROLE)){
            if(!Util.isOwnerOfBook(user, libro)){
                throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
            }
        }
        return true;
    }

    /**
     * Metodo que se encarga de validar la llamada al metodo readCataloguePaginated
     * @param requestLibro
     * @param user
     * @return true si es valido
     */
    private boolean readAll_Validation(RequestLibro requestLibro, User user){
        if(Util.isLargerThanZero(requestLibro.getPagina())){
            return true;
        } else {
            throw new ResponseStatusException(HttpStatus.CONFLICT);
        }
    }

    /**
     * Metodo que revisa que todos los datos que se van a agregar o modificar de un libro sean correctos
     * @param requestLibro
     * @param errorMessage
     * @return true si es valido
     */
    private boolean create_update_RequiredValues(RequestLibro requestLibro, String errorMessage){
        errorMessage = Util.isLengthLessOrEqualThan(requestLibro.getNombre(), 255) ? errorMessage : errorMessage+", Long Name";
        errorMessage = Util.isLengthLessOrEqualThan(requestLibro.getAutor(), 255) ? errorMessage : errorMessage+", Long Author";
        errorMessage = Util.isLengthLessOrEqualThan(requestLibro.getContraportada(), 255) ? errorMessage : errorMessage+", Long Contraportada";

        errorMessage = Util.isNotEmptyString(requestLibro.getNombre()) ? errorMessage : errorMessage+", Empty Name";
        errorMessage = Util.isNotEmptyString(requestLibro.getAutor()) ? errorMessage : errorMessage+", Empty Author";

        errorMessage = Util.isLargerThanZero(requestLibro.getPrecioDia()) ? errorMessage : errorMessage+", Price lower than zero";

        if(!errorMessage.equals("Invalid")){
            return false;
        }
        return true;
    }

    /**
     * Metodo que valida que la calificacion sea mayo a 0 y menor o igual a 5.
     * @param calificacion
     */
    private boolean validateCalificacion(int calificacion){
        if(!Util.isLargerThanZero(calificacion) || !Util.isXLessOrEqualThanY(calificacion, 5)){
            throw new ResponseStatusException(HttpStatus.CONFLICT);
        }
        return true;
    }
}
