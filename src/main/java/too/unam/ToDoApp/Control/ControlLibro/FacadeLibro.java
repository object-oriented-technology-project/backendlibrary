package too.unam.ToDoApp.Control.ControlLibro;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;
import too.unam.ToDoApp.Interface.InterfaceFacadeLibro;
import too.unam.ToDoApp.Interface.InterfaceServiceLibro;
import too.unam.ToDoApp.Model.Libro;
import too.unam.ToDoApp.Util.Util;

@Component
public class FacadeLibro implements InterfaceFacadeLibro {

    @Autowired
    ControllerLibro controllerLibro;

    @Override
    public Libro unsecureRead(int id) {
        return controllerLibro.unsecureRead(id);
    }

    @Override
    public void unsecureCalificarLibro(Libro libro, int calificacion) {
        controllerLibro.unsecureCalificarLibro(libro, calificacion);
    }


}
