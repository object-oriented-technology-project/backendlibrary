package too.unam.ToDoApp.Control.ControlCalificacion;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import too.unam.ToDoApp.Interface.InterfaceServiceCalificacion;
import too.unam.ToDoApp.Model.Calificacion;
import too.unam.ToDoApp.Util.Util;

@Service
public class ControllerCalificacion {

    @Autowired
    private InterfaceServiceCalificacion serviceCalificacion;

    /**
     * Metodo que se encarga de calificar al consumidor, con la calificacion establecida.
     * @param consumidorUsername
     * @param calificacionConsumidor
     */
    public void rate(String consumidorUsername, int calificacionConsumidor) {
        if(validateData(calificacionConsumidor)) {
            Calificacion calificacion = serviceCalificacion.read(consumidorUsername);
            calificacion.setContadorCalificacion(calificacion.getContadorCalificacion() + 1);
            calificacion.setSumatoriaCalificacion(calificacion.getSumatoriaCalificacion() + calificacionConsumidor);
            serviceCalificacion.update(calificacion);
        }
    }

    /**
     * Metodo que valida que la calificacion que se le dara al consumidor sea valida
     * @param calificacionConsumidor
     * @return true si la calificacion es valida
     */
    private boolean validateData(int calificacionConsumidor){
        if(!Util.isLargerThanZero(calificacionConsumidor) || !Util.isXLessOrEqualThanY(calificacionConsumidor, 5)){
            throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE);
        }
        return true;
    }

    public Calificacion get(String consumidorUsername) {
        return serviceCalificacion.read(consumidorUsername);
    }

    public void create(String consumidorUsername) {
        Calificacion calificacion = new Calificacion(consumidorUsername);
        serviceCalificacion.save(calificacion);
    }
}
