package too.unam.ToDoApp.Control.ControlCalificacion;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import too.unam.ToDoApp.Interface.InterfaceFacadeCalificacion;
import too.unam.ToDoApp.Model.Calificacion;

import java.util.ArrayList;
import java.util.List;

@Component
public class FacadeCalificacion implements InterfaceFacadeCalificacion {

    @Autowired
    private ControllerCalificacion controllerCalificacion;

    /**
     * Este metodo solo debe ser llamado cuando un prestador desea calificar a un consumidor.
     * A un consumidor se le califica una vez por prestamo.
     * @param consumidorUsername
     * @param calificacionConsumidor
     */
    @Override
    public void rate(String consumidorUsername, int calificacionConsumidor) {
        controllerCalificacion.rate(consumidorUsername, calificacionConsumidor);
    }

    @Override
    public Calificacion get(String consumidorUsername) {
        return controllerCalificacion.get(consumidorUsername);
    }

    @Override
    public void create(String consumidorUsername) {
        controllerCalificacion.create(consumidorUsername);
    }

    @Override
    public List<Integer> getListaCalificaciones(List<String> consumers) {
        List<Integer> calificaciones = new ArrayList<>();
        for(String consumer: consumers){
            Calificacion calificacion = controllerCalificacion.get(consumer);
            int sumatoriaCalificacion = calificacion.getSumatoriaCalificacion();
            int contadorCalificacion = calificacion.getContadorCalificacion();
            if(contadorCalificacion==0){
                contadorCalificacion=1;
            }
            calificaciones.add(sumatoriaCalificacion/contadorCalificacion);
        }
        return calificaciones;
    }
}
