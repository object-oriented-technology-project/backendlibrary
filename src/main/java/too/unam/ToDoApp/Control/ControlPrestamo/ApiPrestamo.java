package too.unam.ToDoApp.Control.ControlPrestamo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import too.unam.ToDoApp.Interface.InterfaceApiPrestamo;
import too.unam.ToDoApp.Model.DTO.Request.RequestPrestamo;
import too.unam.ToDoApp.Model.DTO.Response.ResponsePrestamoCatalogue;
import too.unam.ToDoApp.Model.DTO.Util.User;

import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/prestamo")
public class ApiPrestamo implements InterfaceApiPrestamo {

    @Autowired
    private ControllerPrestamo controllerPrestamo;

    /**
     * Precondiciones: Rol Consumidor
     * Poscondiciones: Prestamo en estado solicitado
     * @param requestPrestamo
     * @return Ok si la accion se realizo con exito
     */
    @Override
    @RequestMapping(value = {"/solicitar"}, method = RequestMethod.POST)
    public ResponseEntity requestLend(@RequestBody RequestPrestamo requestPrestamo) {
        try {
            final User user = getUser();
            controllerPrestamo.requestLend(requestPrestamo, user);
        } catch (ResponseStatusException exc) {
            throw exc;
        } catch (Exception exc){
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "UNDEFINED ERROR " +exc.getMessage());
        }
        return ResponseEntity.ok("Lend Status: Solicitado");
    }

    /**
     * Precondiciones: Rol Prestador; Prestamo en estado solicitado.
     * Poscondiciones: Prestamo en estado aceptado
     * @param requestPrestamo
     * @return Ok si la accion se realizo con exito
     */
    @Override
    @RequestMapping(value = {"/aceptar"}, method = RequestMethod.PUT)
    public ResponseEntity acceptLend(@RequestBody RequestPrestamo requestPrestamo) {
        try {
            final User user = getUser();
            controllerPrestamo.acceptLend(requestPrestamo, user);
        } catch (ResponseStatusException exc) {
            throw exc;
        } catch (Exception exc){
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "UNDEFINED ERROR " +exc.getMessage());
        }
        return ResponseEntity.ok("Lend Status: Aceptado");
    }

    /**
     * Precondiciones: Rol Prestador; Prestamo en estado solicitado.
     * Poscondiciones: Prestamo en estado rechazado.
     * @param requestPrestamo
     * @return Ok si la accion se realizo con exito
     */
    @Override
    @RequestMapping(value = {"/rechazar"}, method = RequestMethod.PUT)
    public ResponseEntity rejectLend(@RequestBody RequestPrestamo requestPrestamo) {
        try {
            final User user = getUser();
            controllerPrestamo.rejectLend(requestPrestamo, user);
        } catch (ResponseStatusException exc) {
            throw exc;
        } catch (Exception exc){
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "UNDEFINED ERROR " +exc.getMessage());
        }
        return ResponseEntity.ok("Lend Status: Rechazado");
    }

    /**
     * Precondiciones: Rol Prestador; Prestamo en estado aceptado.
     * Poscondiciones: Prestamo en estado devuelto.
     * @param requestPrestamo
     * @return Ok si la accion se realizo con exito
     */
    @Override
    @RequestMapping(value = {"/devolverLibro"}, method = RequestMethod.PUT)
    public ResponseEntity returnBook(@RequestBody RequestPrestamo requestPrestamo) {
        try {
            final User user = getUser();
            controllerPrestamo.returnBook(requestPrestamo, user);
        } catch (ResponseStatusException exc) {
            throw exc;
        } catch (Exception exc){
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "UNDEFINED ERROR " +exc.getMessage());
        }
        return ResponseEntity.ok("Lend Status: Devuelto");
    }

    /**
     * Precondiciones: Rol Prestador; Prestamo en estado Devuelto.
     * Poscondiciones: Prestamo e estado calificado; Calificacion del consumidor actualizada
     * @param requestPrestamo
     * @return Ok si la accion se realizo con exito
     */
    @Override
    @RequestMapping(value = {"/calificarConsumidor"}, method = RequestMethod.PUT)
    public ResponseEntity rateConsumer(@RequestBody RequestPrestamo requestPrestamo) {
        try {
            final User user = getUser();
            controllerPrestamo.rateConsumer(requestPrestamo, user);
        } catch (ResponseStatusException exc) {
            throw exc;
        } catch (Exception exc){
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "UNDEFINED ERROR " +exc.getMessage());
        }
        return ResponseEntity.ok("Lend Status: Calificado");
    }

    /**
     * Precondiciones: Rol Consumidor; Prestamo en estado devuelto o calificado.
     * Poscondiciones: Calificacion del libro actualizada
     * @param requestPrestamo
     * @return Ok si la accion se realizo con exito
     */
    @Override
    @RequestMapping(value = {"/calificarLibro"}, method = RequestMethod.PUT)
    public ResponseEntity rateBook(@RequestBody RequestPrestamo requestPrestamo) {
        try {
            final User user = getUser();
            controllerPrestamo.rateBook(requestPrestamo, user);
        } catch (ResponseStatusException exc) {
            throw exc;
        } catch (Exception exc){
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "UNDEFINED ERROR " +exc.getMessage());
        }
        return ResponseEntity.ok("Book Rated");
    }

    /**
     * Precondiciones: Usuario loggeado
     * Poscondiciones: El usuario recibira los prestamos que le conciernen.
     * Detalle: Si el usuario es de rol Consumidor, recibira todos los prestamos que ha pedido o realizado.
     * Si el usuario es de rol Prestador, recibira todos los prestamos de los libros que le pertenecen.
     * @return Lista de Prestamos
     */
    @Override
    @RequestMapping(value = {"/catalogo"}, method = RequestMethod.GET)
    public ResponseEntity getAll() {
        try {
            final User user = getUser();
            List<ResponsePrestamoCatalogue> response = controllerPrestamo.getAll(user);
            return ResponseEntity.ok(response);
        } catch (ResponseStatusException exc) {
            throw exc;
        } catch (Exception exc){
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "UNDEFINED ERROR " +exc.getMessage());
        }
    }

    /**
     * Obtiene el usuario de los valores de SpringSecurity
     * @return
     */
    private User getUser(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        final String username = (String) authentication.getPrincipal();
        final Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
        GrantedAuthority authority = (GrantedAuthority)authorities.toArray()[0];
        return new User(username, authority.getAuthority());
    }
}
