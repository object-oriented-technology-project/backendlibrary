package too.unam.ToDoApp.Control.ControlPrestamo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import too.unam.ToDoApp.Interface.InterfaceFacadeCalificacion;
import too.unam.ToDoApp.Interface.InterfaceFacadeLibro;
import too.unam.ToDoApp.Interface.InterfaceServicePrestamo;
import too.unam.ToDoApp.Model.DTO.Request.RequestPrestamo;
import too.unam.ToDoApp.Model.DTO.Response.ResponsePrestamoCatalogue;
import too.unam.ToDoApp.Model.DTO.Util.User;
import too.unam.ToDoApp.Model.Enumerated.EstadoPrestamo;
import too.unam.ToDoApp.Model.Libro;
import too.unam.ToDoApp.Model.Prestamo;
import too.unam.ToDoApp.Util.JwtUtil;
import too.unam.ToDoApp.Util.Util;

import java.util.ArrayList;
import java.util.List;

@Service
public class ControllerPrestamo {

    @Autowired
    private InterfaceFacadeLibro facadeLibro;

    @Autowired
    private InterfaceServicePrestamo servicePrestamo;

    @Autowired
    private InterfaceFacadeCalificacion facadeCalificacion;

    /**
     * Este metodo se encarga de guardar la solicitud de prestamo hecha por el Consumidor
     * @param requestPrestamo
     * @param user
     */
    public void requestLend(RequestPrestamo requestPrestamo, User user) {
        if(requestLend_validation(requestPrestamo, user)){
            if(facadeCalificacion.get(user.getUsername())==null){
                facadeCalificacion.create(user.getUsername());
            }
            final Libro libro = facadeLibro.unsecureRead(requestPrestamo.getLibroId());
            Prestamo prestamo = new Prestamo(
                    libro,
                    user.getUsername(),
                    requestPrestamo.getCantidadDias(),
                    libro.getPrecioDia()
            );
            servicePrestamo.save(prestamo);
        }
    }

    /**
     * Este metodo se encarga de que el Prestador acepte la solicitud de prestamo
     * @param requestPrestamo
     * @param user
     */
    public void acceptLend(RequestPrestamo requestPrestamo, User user) {
        Prestamo prestamo = servicePrestamo.read(requestPrestamo.getId());
        if(lend_dataValidation(user, prestamo)){
            prestamo = aceptarPrestamo(prestamo);
            servicePrestamo.update(prestamo);
        }
    }

    /**
     * Este metodo se encarga de que el prestador rechaze la solicitud de prestamo
     * @param requestPrestamo
     * @param user
     */
    public void rejectLend(RequestPrestamo requestPrestamo, User user) {
        Prestamo prestamo = servicePrestamo.read(requestPrestamo.getId());
        if(lend_dataValidation(user, prestamo)){
            prestamo = rechazarPrestamo(prestamo);
            servicePrestamo.update(prestamo);
        }
    }

    /**
     * Este metodo se encarga de que el prestador actualice que el libro le ha sido devuelto
     * @param requestPrestamo
     * @param user
     */
    public void returnBook(RequestPrestamo requestPrestamo, User user) {
        Prestamo prestamo = servicePrestamo.read(requestPrestamo.getId());
        if(lend_dataValidation(user, prestamo)){
            prestamo = devolverLibroPrestamo(prestamo);
            servicePrestamo.update(prestamo);
        }
    }

    /**
     * Este metodo permite que el prestador califique al consumidor de su libro
     * @param requestPrestamo
     * @param user
     */
    public void rateConsumer(RequestPrestamo requestPrestamo, User user) {
        Prestamo prestamo = servicePrestamo.read(requestPrestamo.getId());
        if(lend_dataValidation(user, prestamo)){
            facadeCalificacion.rate(prestamo.getConsumidorUsername(), requestPrestamo.getCalificacionConsumidor());
            prestamo = calificarConsumidorPrestamo(prestamo);
            servicePrestamo.update(prestamo);
        }
    }

    /**
     * Este metodo se encarga de que, ya devuelto el libro, el consumidor califique el libro.
     * @param requestPrestamo
     * @param user
     */
    public void rateBook(RequestPrestamo requestPrestamo, User user) {
        Prestamo prestamo = servicePrestamo.read(requestPrestamo.getId());
        if (this.bookRate_validation(prestamo, user)){
            facadeLibro.unsecureCalificarLibro(prestamo.getLibro(), requestPrestamo.getCalificacionLibro());
            prestamo.setBookRated(true);
            servicePrestamo.update(prestamo);
        }
    }

    /**
     * Este metodo regresa todos los prestamos del consumidor o prestador.
     * @param user
     */
    public List<ResponsePrestamoCatalogue> getAll(User user) {
        List<Prestamo> prestamos = null;
        List<String> consumers = null;
        List<Integer> calificacionesConsumers = null;
        if(user.getRole().equals(JwtUtil.CONSUMER_ROLE)){
            prestamos = servicePrestamo.getAllPrestamosConsumer(user.getUsername());
        } else if (user.getRole().equals(JwtUtil.LENDER_ROLE)){
            prestamos = servicePrestamo.getAllPrestamosLender(user.getUsername());
            consumers = consumerUsernamesFromLends(prestamos);
            calificacionesConsumers = facadeCalificacion.getListaCalificaciones(consumers);
            return ResponsePrestamoCatalogue.PrestamosToResponsePrestamoCatalogue(prestamos, calificacionesConsumers);
        }
        if(prestamos==null){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return ResponsePrestamoCatalogue.PrestamosToResponsePrestamoCatalogue(prestamos);
    }

    //----------------------------------METODOS PRIVADOS--------------------------------------

    /**
     * Metodo recibe un prestamo y regresa un prestamo en estado aceptado.
     * @param prestamo
     * @return Prestamo aceptado
     */
    private Prestamo aceptarPrestamo(Prestamo prestamo){
        if(!prestamo.getEstado().equals(EstadoPrestamo.SOLICITADO)){
            throw new ResponseStatusException(HttpStatus.PRECONDITION_FAILED);
        }
        prestamo.setEstado(EstadoPrestamo.ACEPTADO);
        return prestamo;
    }

    /**
     * Metodo recibe un prestamo y regresa un prestamo en estado rechazado.
     * @param prestamo
     * @return Prestamo rechazado
     */
    private Prestamo rechazarPrestamo(Prestamo prestamo){
        if(!prestamo.getEstado().equals(EstadoPrestamo.SOLICITADO)){
            throw new ResponseStatusException(HttpStatus.PRECONDITION_FAILED);
        }
        prestamo.setEstado(EstadoPrestamo.RECHAZADO);
        return prestamo;
    }

    /**
     * Metodo que recibe un prestamo y regresa un prestamo en estado devuelto.
     * @param prestamo
     * @return Prestamo Devuelto
     */
    private Prestamo devolverLibroPrestamo(Prestamo prestamo){
        if(!prestamo.getEstado().equals(EstadoPrestamo.ACEPTADO)){
            throw new ResponseStatusException(HttpStatus.PRECONDITION_FAILED);
        }
        prestamo.setEstado(EstadoPrestamo.DEVUELTO);
        return prestamo;
    }

    /**
     * Metodo que recibe un prestamo y regresa un prestamo en estado calificado
     * @param prestamo
     * @return Prestamo calificado
     */
    private Prestamo calificarConsumidorPrestamo(Prestamo prestamo){
        if(!prestamo.getEstado().equals(EstadoPrestamo.DEVUELTO)){
            throw new ResponseStatusException(HttpStatus.PRECONDITION_FAILED);
        }
        prestamo.setEstado(EstadoPrestamo.CALIFICADO);
        return prestamo;
    }

    /**
     * Metodo entra en accion cuando se solicita un prestamo, realiza validaciones.
     * @param requestPrestamo
     * @param user
     * @return true si es valido
     * @throws ResponseStatusException
     */
    private boolean requestLend_validation(RequestPrestamo requestPrestamo, User user) throws ResponseStatusException{
        if(!user.getRole().equals(JwtUtil.CONSUMER_ROLE)){
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
        }
        String errorMessage = "Invalid";
        if(!requestLend_RequiredValues(requestPrestamo, errorMessage)){
            throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE);
        }
        return true;
    }

    /**
     * Metodo entra en accion cuando se desea calificar un libro.
     * @param prestamo
     * @param user
     * @return true si es valido
     * @throws ResponseStatusException
     */
    private boolean bookRate_validation(Prestamo prestamo, User user) throws ResponseStatusException{
        if(!prestamo.getConsumidorUsername().equals(user.getUsername())){
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
        }
        if (!(prestamo.getEstado().equals(EstadoPrestamo.DEVUELTO) || prestamo.getEstado().equals(EstadoPrestamo.CALIFICADO))){
            throw new ResponseStatusException(HttpStatus.PRECONDITION_FAILED);
        }
        return true;
    }

    /**
     * Metodo que entra en accion cuando se quiere cambiar de estado un prestamo, realiza validaciones.
     * @param user
     * @param prestamo
     * @return true si es valido
     */
    private boolean lend_dataValidation(User user, Prestamo prestamo){
        if(prestamo == null){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        if(!Util.isOwnerOfBook(user, prestamo.getLibro())){
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
        }
        return true;
    }


    /**
     * Metodo que valida que, al solicitar un prestamo, los datos del prestamo sean correctos.
     * @param requestPrestamo
     * @param errorMessage
     * @return true si los datos son validos
     */
    private boolean requestLend_RequiredValues(RequestPrestamo requestPrestamo, String errorMessage){
        errorMessage = Util.isLargerThanZero(requestPrestamo.getCantidadDias()) ? errorMessage : errorMessage+", DAYS QUANTITY LOWER THAN ZERO";
        if(!errorMessage.equals("Invalid")){
            return false;
        }
        return true;
    }

    private List<String> consumerUsernamesFromLends(List<Prestamo> prestamos){
        List<String> usernames = new ArrayList<>();
        for (Prestamo prestamo: prestamos) {
            usernames.add(prestamo.getConsumidorUsername());
        }
        return usernames;
    }
}
