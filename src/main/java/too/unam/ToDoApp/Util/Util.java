package too.unam.ToDoApp.Util;

import too.unam.ToDoApp.Model.DTO.Util.User;
import too.unam.ToDoApp.Model.Libro;

public class Util {

    public static String API_LIBRO = "/libros";
    public static String LIBRO_CREATE = "/libro";
    public static String LIBRO_UPDATE = "/libro";
    public static String LIBRO_DELETE = "/libro";
    public static String LIBRO_READ = "/libro";
    public static String LIBRO_READ_CATALOGUE = "/catalogo";

    public static String API_PRESTAMO = "/prestamo";
    public static String PRESTAMO_SOLICITAR = "/solicitar";
    public static String PRESTAMO_ACEPTAR = "/aceptar";
    public static String PRESTAMO_RECHAZAR = "/rechazar";
    public static String PRESTAMO_DEVOLVER = "/devolverLibro";
    public static String PRESTAMO_CALIFICAR_CONSUMIDOR = "/calificarConsumidor";
    public static String PRESTAMO_CALIFICAR_LIBRO = "/calificarLibro";
    public static String PRESTAMO_CATALOGO = "/catalogo";

    public static boolean isLengthLessOrEqualThan(String s, int n){
        return s.length()<=n;
    }

    public static boolean isNotEmptyString(String s){
        return s.replace(" ", "").length() > 0;
    }

    public static boolean isConsumer(User user){
        return (user.getRole().equals(JwtUtil.CONSUMER_ROLE));
    }

    public static boolean isLender(User user){
        return (user.getRole().equals(JwtUtil.LENDER_ROLE));
    }

    public static boolean isLargerThanZero(int n){
        return n>0;
    }

    public static boolean isXLessOrEqualThanY(int x, int y){ return x<=y;}

    public static boolean isOwnerOfBook(User user, Libro libro){
        return (user.getUsername().equals(libro.getPrestadorUsername()));
    }
}
