package too.unam.ToDoApp.Util;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import too.unam.ToDoApp.Model.DTO.Util.User;

import javax.servlet.http.Cookie;
import java.util.Date;
import java.util.function.Function;

@Service
public class JwtUtil {

    private String SECRET_KEY = "secret";
    public static final String BEARER_ = "BEARER_";
    public static final String AUTHORIZATION = "AUTHORIZATION";
    public static final String AUTHORITIES = "authorities";
    public static final String CONSUMER_ROLE = "CONSUMER";
    public static final String LENDER_ROLE = "LENDER";

    public String getTokenFromHeader(String authorizationHeader){
        return authorizationHeader.replace(BEARER_, "");
    }

    /**
     *
     * @param jwtToken
     * @return
     */
    public Claims validateToken(String jwtToken) {

        return Jwts.parser().setSigningKey(SECRET_KEY.getBytes()).parseClaimsJws(jwtToken).getBody();
    }

    /**
     * Takes the token and return user name
     * @param token
     * @return username
     */
    public String extractUsername(String token) {
        return extractClaim(token, Claims::getSubject);
    }

    /**
     * Takes the token and return the expiration date
     * @return expiration date
     */
    public Date extractExpiration(String token) {
        return extractClaim(token, Claims::getExpiration);
    }

    /**
     * Takes a token and get Claims out of it
     * @param token
     * @param claimsResolver
     * @param <T>
     * @return
     */
    public <T> T extractClaim(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = extractAllClaims(token);
        return claimsResolver.apply(claims);
    }

    private Claims extractAllClaims(String token) {
        return Jwts.parser().setSigningKey(SECRET_KEY).parseClaimsJws(token).getBody();
    }

    /**
     *
     * @param token
     * @return true if the token is expired, false otherwiser
     */
    private Boolean isTokenExpired(String token) {
        return extractExpiration(token).before(new Date());
    }

    public Cookie getAuthorizationCookie(Cookie[] cookies){
        for (Cookie cookie: cookies) {
            if(cookie.getName().equals(JwtUtil.AUTHORIZATION) && cookie.getValue().startsWith(JwtUtil.BEARER_)){
                return cookie;
            }
        }
        return null;
    }

}
