package too.unam.ToDoApp.Model;

import too.unam.ToDoApp.Model.Enumerated.EstadoPrestamo;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="prestamo")
public class Prestamo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Libro libro;

    @Column(name="consumidor_username", nullable = false)
    private String consumidorUsername;

    @Column(name="cantidad_dias", nullable = false)
    private int cantidadDias;

    @Column(name="precio_dia", nullable = false)
    private int precioDia;

    @Enumerated(EnumType.STRING)
    @Column(name="estado", nullable = false)
    private EstadoPrestamo estado;

    @Column(name="fecha_prestamo", nullable = false)
    private Date fechaPrestamo;

    @Column(name="is_book_rated", nullable = false)
    private boolean isBookRated = false;

    public Prestamo() {
    }

    public Prestamo(Libro libro, String consumidorUsername, int cantidadDias, int precioDia) {
        this.libro = libro;
        this.consumidorUsername = consumidorUsername;
        this.cantidadDias = cantidadDias;
        this.precioDia = precioDia;
        this.estado = EstadoPrestamo.SOLICITADO;
        this.fechaPrestamo = new Date();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Libro getLibro() {
        return libro;
    }

    public void setLibro(Libro libro) {
        this.libro = libro;
    }

    public String getConsumidorUsername() {
        return consumidorUsername;
    }

    public void setConsumidorUsername(String consumidorUsername) {
        this.consumidorUsername = consumidorUsername;
    }

    public int getCantidadDias() {
        return cantidadDias;
    }

    public void setCantidadDias(int cantidadDias) {
        this.cantidadDias = cantidadDias;
    }

    public int getPrecioDia() {
        return precioDia;
    }

    public void setPrecioDia(int precioDia) {
        this.precioDia = precioDia;
    }

    public EstadoPrestamo getEstado() {
        return estado;
    }

    public void setEstado(EstadoPrestamo estado) {
        this.estado = estado;
    }

    public Date getFechaPrestamo() {
        return fechaPrestamo;
    }

    public void setFechaPrestamo(Date fechaPrestamo) {
        this.fechaPrestamo = fechaPrestamo;
    }

    public boolean isBookRated() {
        return isBookRated;
    }

    public void setBookRated(boolean bookRated) {
        isBookRated = bookRated;
    }
}