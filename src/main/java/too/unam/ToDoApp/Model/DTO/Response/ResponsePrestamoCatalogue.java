package too.unam.ToDoApp.Model.DTO.Response;

import too.unam.ToDoApp.Model.Enumerated.EstadoPrestamo;
import too.unam.ToDoApp.Model.Prestamo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ResponsePrestamoCatalogue{

    private int id;
    private String consumidorUsername;
    private int cantidadDias;
    private int precioDia;
    private String libro;
    private String estado;
    private Date fechaPrestamo;
    private int calificacionConsumer;
    private boolean isBookRated;
    private int bookRate;

    public ResponsePrestamoCatalogue(int id, String consumidorUsername, int cantidadDias, int precioDia, String estado, Date fechaPrestamo, String libro, boolean isBookRated) {
        this.id = id;
        this.consumidorUsername = consumidorUsername;
        this.cantidadDias = cantidadDias;
        this.precioDia = precioDia;
        this.estado = estado;
        this.fechaPrestamo = fechaPrestamo;
        this.libro = libro;
        this.isBookRated=isBookRated;
    }

    public ResponsePrestamoCatalogue(int id, String consumidorUsername, int cantidadDias, int precioDia, String estado, Date fechaPrestamo, String libro, int calificacionConsumer) {
        this.id = id;
        this.consumidorUsername = consumidorUsername;
        this.cantidadDias = cantidadDias;
        this.precioDia = precioDia;
        this.estado = estado;
        this.fechaPrestamo = fechaPrestamo;
        this.libro = libro;
        this.calificacionConsumer=calificacionConsumer;
        this.bookRate=0;
    }

    /**
     * Catalogo del Consumidor
     * @param prestamos
     * @return
     */
    public static List<ResponsePrestamoCatalogue> PrestamosToResponsePrestamoCatalogue(List<Prestamo> prestamos){
        List<ResponsePrestamoCatalogue> response = new ArrayList<>();

        for (Prestamo prestamo: prestamos) {
            response.add(new ResponsePrestamoCatalogue(
                    prestamo.getId(),
                    prestamo.getConsumidorUsername(),
                    prestamo.getCantidadDias(),
                    prestamo.getPrecioDia(),
                    prestamo.getEstado().toString(),
                    prestamo.getFechaPrestamo(),
                    prestamo.getLibro().getNombre(),
                    prestamo.isBookRated()
            ));
        }
        return response;
    }

    /**
     * Catalogo del Lender
     * @param prestamos
     * @param calificaciones
     * @return
     */
    public static List<ResponsePrestamoCatalogue> PrestamosToResponsePrestamoCatalogue(List<Prestamo> prestamos, List<Integer> calificaciones){
        List<ResponsePrestamoCatalogue> response = new ArrayList<>();
        for(int i=0; i<prestamos.size(); i++){
            response.add(new ResponsePrestamoCatalogue(
                    prestamos.get(i).getId(),
                    prestamos.get(i).getConsumidorUsername(),
                    prestamos.get(i).getCantidadDias(),
                    prestamos.get(i).getPrecioDia(),
                    prestamos.get(i).getEstado().toString(),
                    prestamos.get(i).getFechaPrestamo(),
                    prestamos.get(i).getLibro().getNombre(),
                    calificaciones.get(i)
            ));
        }
        return response;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getConsumidorUsername() {
        return consumidorUsername;
    }

    public void setConsumidorUsername(String consumidorUsername) {
        this.consumidorUsername = consumidorUsername;
    }

    public int getCantidadDias() {
        return cantidadDias;
    }

    public void setCantidadDias(int cantidadDias) {
        this.cantidadDias = cantidadDias;
    }

    public int getPrecioDia() {
        return precioDia;
    }

    public void setPrecioDia(int precioDia) {
        this.precioDia = precioDia;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Date getFechaPrestamo() {
        return fechaPrestamo;
    }

    public void setFechaPrestamo(Date fechaPrestamo) {
        this.fechaPrestamo = fechaPrestamo;
    }

    public String getLibro() {
        return libro;
    }

    public void setLibro(String libro) {
        this.libro = libro;
    }

    public int getCalificacionConsumer() {
        return calificacionConsumer;
    }

    public void setCalificacionConsumer(int calificacionConsumer) {
        this.calificacionConsumer = calificacionConsumer;
    }

    public boolean isBookRated() {
        return isBookRated;
    }

    public void setBookRated(boolean bookRated) {
        isBookRated = bookRated;
    }

    public int getBookRate() {
        return bookRate;
    }

    public void setBookRate(int bookRate) {
        this.bookRate = bookRate;
    }
}
