package too.unam.ToDoApp.Model.DTO.Request;

import too.unam.ToDoApp.Model.Enumerated.EstadoPrestamo;

public class RequestPrestamo {

    private int id;
    private int libroId;
    private int cantidadDias;
    private int calificacionConsumidor;
    private int calificacionLibro;

    public RequestPrestamo() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getLibroId() {
        return libroId;
    }

    public void setLibroId(int libroId) {
        this.libroId = libroId;
    }

    public int getCantidadDias() {
        return cantidadDias;
    }

    public void setCantidadDias(int cantidadDias) {
        this.cantidadDias = cantidadDias;
    }

    public int getCalificacionConsumidor() {
        return calificacionConsumidor;
    }

    public void setCalificacionConsumidor(int calificacionConsumidor) {
        this.calificacionConsumidor = calificacionConsumidor;
    }

    public int getCalificacionLibro() {
        return calificacionLibro;
    }

    public void setCalificacionLibro(int calificacionLibro) {
        this.calificacionLibro = calificacionLibro;
    }
}
