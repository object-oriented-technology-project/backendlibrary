package too.unam.ToDoApp.Model.DTO.Request;

import org.springframework.web.multipart.MultipartFile;

import java.io.File;

public class RequestLibro {

    //ATRIBUTOS DE LIBRO
    private int id;
    private String nombre;
    private String autor;
    private String contraportada;
    private MultipartFile imagen;
    private int precioDia;
    private int sumatoriaCalificacion;
    private int contadorCalificacion;

    //ATRIBUTOS DE PAGINADO
    private int pagina;

    public RequestLibro() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        if(nombre==null)
            return "";
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getAutor() {
        if(autor==null)
            return "";
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getContraportada() {
        if(contraportada == null){
            return "";
        }
        return contraportada;
    }

    public void setContraportada(String contraportada) {
        this.contraportada = contraportada;
    }

    public MultipartFile getImagen() {
        return imagen;
    }

    public void setImagen(MultipartFile imagen) {
        this.imagen = imagen;
    }

    public int getPrecioDia() {
        return precioDia;
    }

    public void setPrecioDia(int precioDia) {
        this.precioDia = precioDia;
    }

    public int getSumatoriaCalificacion() {
        return sumatoriaCalificacion;
    }

    public void setSumatoriaCalificacion(int sumatoriaCalificacion) {
        this.sumatoriaCalificacion = sumatoriaCalificacion;
    }

    public int getContadorCalificacion() {
        return contadorCalificacion;
    }

    public void setContadorCalificacion(int contadorCalificacion) {
        this.contadorCalificacion = contadorCalificacion;
    }

    public int getPagina() {
        return pagina;
    }

    public void setPagina(int pagina) {
        this.pagina = pagina;
    }
}
