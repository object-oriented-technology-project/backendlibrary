package too.unam.ToDoApp.Model.DTO.Response;

import too.unam.ToDoApp.Model.Libro;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ResponseLibroCatalogue{
    private int id;
    private String nombre;
    private String autor;
    private String portadaImagenUrl;

    public ResponseLibroCatalogue() {
    }

    public ResponseLibroCatalogue(int id, String nombre, String autor, String portadaImagenUrl) {
        this.id = id;
        this.nombre = nombre;
        this.autor = autor;
        this.portadaImagenUrl = portadaImagenUrl;
    }

    /**
     * Metodo que convierte una lista de libros en una lista de ResponseLibroCatalogue
     * @param libros
     * @return
     */
    public static List<ResponseLibroCatalogue> LibrosToResponseLibroCatalogue(List<Libro> libros){
        List<ResponseLibroCatalogue> librosCatalogue = new ArrayList<>();
        for (Libro libro:libros) {
            librosCatalogue.add(new ResponseLibroCatalogue(
               libro.getId(),
               libro.getNombre(),
               libro.getAutor(),
               libro.getPortadaImagenUrl()
            ));
        }
        return librosCatalogue;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getPortadaImagenUrl() {
        return portadaImagenUrl;
    }

    public void setPortadaImagenUrl(String portadaImagenUrl) {
        this.portadaImagenUrl = portadaImagenUrl;
    }
}
