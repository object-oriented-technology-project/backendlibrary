package too.unam.ToDoApp.Model.Enumerated;

public enum EstadoPrestamo {
    SOLICITADO,
    RECHAZADO,
    ACEPTADO,
    DEVUELTO,
    CALIFICADO
}
