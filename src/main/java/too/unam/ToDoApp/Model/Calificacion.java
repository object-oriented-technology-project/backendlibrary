package too.unam.ToDoApp.Model;

import javax.persistence.*;

@Entity
@Table(name="calificacion")
public class Calificacion {

    @Id
    @Column(name= "consumidorUsername")
    private String consumidorUsername;

    @Column(name="sumatoria", nullable = false)
    private int sumatoriaCalificacion = 0;

    @Column(name="contador", nullable = false)
    private int contadorCalificacion = 0;

    public Calificacion() {
    }

    public Calificacion(String consumidorUsername) {
        this.consumidorUsername = consumidorUsername;
        this.sumatoriaCalificacion=0;
        this.contadorCalificacion=0;
    }

    public String getConsumidorUsername() {
        return consumidorUsername;
    }

    public void setConsumidorUsername(String consumidorUsername) {
        this.consumidorUsername = consumidorUsername;
    }

    public int getSumatoriaCalificacion() {
        return sumatoriaCalificacion;
    }

    public void setSumatoriaCalificacion(int sumatoriaCalificacion) {
        this.sumatoriaCalificacion = sumatoriaCalificacion;
    }

    public int getContadorCalificacion() {
        return contadorCalificacion;
    }

    public void setContadorCalificacion(int contadorCalificacion) {
        this.contadorCalificacion = contadorCalificacion;
    }
}
