package too.unam.ToDoApp.Model;

import javax.persistence.*;

@Entity
@Table(name="libro")
public class Libro {

    public Libro() {
    }

    public Libro(String nombre, String autor, String contraportada, String portadaImagenUrl,
                 String username, int precioDia, int sumatoriaCalificacion, int contadorCalificacion) {
        this.nombre = nombre;
        this.autor = autor;
        this.contraportada = contraportada;
        this.portadaImagenUrl = portadaImagenUrl;
        this.prestadorUsername = username;
        this.precioDia = precioDia;
        this.sumatoriaCalificacion = sumatoriaCalificacion;
        this.contadorCalificacion = contadorCalificacion;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name="nombre", nullable = false)
    private String nombre;

    @Column(name="autor", nullable = false)
    private String autor;

    @Column(name="contraportada", nullable = false)
    private String contraportada;

    @Column(name="portada_imagen_url", nullable = false)
    private String portadaImagenUrl;

    @Column(name="prestador_username", nullable = false)
    private String prestadorUsername;

    @Column(name="precio_dia", nullable = false)
    private int precioDia;

    @Column(name="sumatoria_calificacion", nullable = false)
    private int sumatoriaCalificacion;

    @Column(name="contador_calificacion", nullable = false)
    private int contadorCalificacion;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getContraportada() {
        return contraportada;
    }

    public void setContraportada(String contraportada) {
        this.contraportada = contraportada;
    }

    public String getPortadaImagenUrl() {
        return portadaImagenUrl;
    }

    public void setPortadaImagenUrl(String portadaImagenUrl) {
        this.portadaImagenUrl = portadaImagenUrl;
    }

    public String getPrestadorUsername() {
        return prestadorUsername;
    }

    public void setPrestadorUsername(String prestadorusername) {
        this.prestadorUsername = prestadorusername;
    }

    public int getPrecioDia() {
        return precioDia;
    }

    public void setPrecioDia(int precioDia) {
        this.precioDia = precioDia;
    }

    public int getSumatoriaCalificacion() {
        return sumatoriaCalificacion;
    }

    public void setSumatoriaCalificacion(int sumatoriaCalificacion) {
        this.sumatoriaCalificacion = sumatoriaCalificacion;
    }

    public int getContadorCalificacion() {
        return contadorCalificacion;
    }

    public void setContadorCalificacion(int contadorCalificacion) {
        this.contadorCalificacion = contadorCalificacion;
    }
}
