package too.unam.ToDoApp.Persistence.Generic;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import too.unam.ToDoApp.Interface.InterfaceGenericService;

import javax.persistence.criteria.*;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import org.hibernate.query.Query;
import java.util.List;

@Repository
@Transactional
public abstract class GenericService<U> implements InterfaceGenericService<U> {

    private Class<U> type;

    public GenericService(){
        Type t = getClass().getGenericSuperclass();
        ParameterizedType pt = (ParameterizedType)t;
        this.type = (Class) pt.getActualTypeArguments()[0];
    }

    @Autowired
    protected SessionFactory sessionFactory;

    @Override
    public U read(int id) {
        return sessionFactory.getCurrentSession().get(type, id);
    }

    @Override
    public List<U> readAll() {
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<U> criteria = builder.createQuery(type);
        Root<U> root = criteria.from(type);
        criteria.select(root);
        Query<U> query = session.createQuery(criteria);
        List<U> entities = query.getResultList();

        return entities;
    }

    @Override
    public void save(U entity) {
        sessionFactory.getCurrentSession().save(entity);
    }

    @Override
    public void update(U entity) {
        sessionFactory.getCurrentSession().update(entity);
    }

    @Override
    public void delete(U entity) {
        sessionFactory.getCurrentSession().delete(entity);
    }
}
