package too.unam.ToDoApp.Persistence;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import too.unam.ToDoApp.Interface.InterfaceServiceCalificacion;
import too.unam.ToDoApp.Model.Calificacion;
import too.unam.ToDoApp.Persistence.Generic.GenericService;

@Repository
@Transactional
public class ServiceCalificacion extends GenericService<Calificacion> implements InterfaceServiceCalificacion {

    @Autowired
    private SessionFactory sessionFactory;

    public ServiceCalificacion(){
        super();
    }

    public Calificacion read(String username){
        return sessionFactory.getCurrentSession().get(Calificacion.class, username);
    }
}
