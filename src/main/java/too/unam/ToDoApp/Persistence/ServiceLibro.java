package too.unam.ToDoApp.Persistence;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import too.unam.ToDoApp.Interface.InterfaceServiceLibro;
import too.unam.ToDoApp.Model.Libro;
import too.unam.ToDoApp.Persistence.Generic.GenericService;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
@Transactional
public class ServiceLibro extends GenericService<Libro> implements InterfaceServiceLibro {

    public ServiceLibro(){
        super();
    }

    @Override
    public List<Libro> readCataloguePaginatedLender(String username, int page, String book, String author) {
        validFilters(book, author);
        int firstResult = (page-1)*18;

        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<Libro> criteria = builder.createQuery(Libro.class);
        Root<Libro> root = criteria.from(Libro.class);

        criteria.select(root)
                .where(builder.and(
                        builder.equal(root.get("prestadorUsername"), username),
                        builder.like(root.get("autor"), "%" +author +"%"),
                        builder.like(root.get("nombre"), "%" +book +"%")
                ));

        Query<Libro> query = session.createQuery(criteria);
        query.setFirstResult(firstResult);
        query.setMaxResults(18);
        List<Libro> libros = query.getResultList();

        return libros;
    }

    @Override
    public List<Libro> readCataloguePaginatedConsumer(int page, String book, String author) {
        validFilters(book, author);
        int firstResult = (page-1)*18;
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<Libro> criteria = builder.createQuery(Libro.class);
        Root<Libro> root = criteria.from(Libro.class);

        criteria.select(root)
                .where(builder.and(
                        builder.like(root.get("autor"), "%" +author +"%"),
                        builder.like(root.get("nombre"), "%" +book +"%")
                ));

        Query<Libro> query = session.createQuery(criteria);
        query.setFirstResult(firstResult);
        query.setMaxResults(18);
        List<Libro> libros = query.getResultList();

        return libros;
    }

    private void validFilters(String author, String book){
        if(book==null){
            book = "";
        }
        if(author==null){
            author="";
        }
    }
}
