package too.unam.ToDoApp.Persistence;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import too.unam.ToDoApp.Interface.InterfaceServicePrestamo;
import too.unam.ToDoApp.Model.DTO.Util.User;
import too.unam.ToDoApp.Model.Libro;
import too.unam.ToDoApp.Model.Prestamo;
import too.unam.ToDoApp.Persistence.Generic.GenericService;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

@Repository
@Transactional
public class ServicePrestamo extends GenericService<Prestamo> implements InterfaceServicePrestamo {

    @Autowired
    private SessionFactory sessionFactory;


    public ServicePrestamo(){
        super();
    }


    @Override
    public List<Prestamo> getAllPrestamosConsumer(String username) {
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<Prestamo> criteria = builder.createQuery(Prestamo.class);
        Root<Prestamo> root = criteria.from(Prestamo.class);
        criteria.select(root)
                .where(builder.equal(root.get("consumidorUsername"), username));
        Query<Prestamo> query = session.createQuery(criteria);
        List<Prestamo> prestamos = query.getResultList();

        return prestamos;
    }

    @Override
    public List<Prestamo> getAllPrestamosLender(String username) {
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<Prestamo> criteria = builder.createQuery(Prestamo.class);
        Root<Prestamo> root = criteria.from(Prestamo.class);

        Join<Prestamo, Libro> joinLibro = root.join("libro");

        List<Predicate> conditions = new ArrayList<>();

        conditions.add(builder.and(
                builder.equal(joinLibro.get("prestadorUsername"), username)
        ));

        criteria.where(conditions.toArray(new Predicate[] {}));

        Query<Prestamo> query = session.createQuery(criteria);
        List<Prestamo> prestamos = query.getResultList();

        return prestamos;
    }
}
